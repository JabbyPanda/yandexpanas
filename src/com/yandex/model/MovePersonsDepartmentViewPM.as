package com.yandex.model
{
	import com.yandex.model.vo.DepartmentVO;
	import com.yandex.model.vo.PersonVO;

	import mx.events.CloseEvent;
	import mx.utils.StringUtil;

	public class MovePersonsDepartmentViewPM
	{
		[Inject]
		[Bindable]
		public var model:PersonsModel;

		[Bindable]
		public var movePersonsPrompt:String;

		[Bindable]
		public var selectedDepartment:DepartmentVO;

		private var _range:String;

		public function get range():String
		{
			return _range;
		}

		public function set range(value:String):void
		{
			_range = value;
			var numberOfItems:Number = persons.length;
			movePersonsPrompt = numberOfItems > 0
					? StringUtil.substitute("Move selected {0} persons", numberOfItems)
					: "Move selected person";
		}

		public function get persons():Array
		{
			return (range == PersonsModel.ALL_RANGE)
					? model.persons.toArray()
					: model.selectedItems.toArray();
		}

		public function submit():void
		{
			if (range == PersonsModel.ALL_RANGE)
			{
				model.moveAllPersons(selectedDepartment);
			}
			else
			{
				model.movePersons(persons, selectedDepartment);
			}

			close();
		}

		public function close():void
		{
			selectedDepartment = null;
			dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
		}

		public function isValidSelection(selectedDepartment:DepartmentVO):Boolean
		{
			return selectedDepartment &&
					persons.length &&
					(persons[0] as PersonVO).departmentID  != selectedDepartment.id;
		}
	}
}
