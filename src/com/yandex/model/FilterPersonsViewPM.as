package com.yandex.model
{
	import com.yandex.model.vo.PersonVOMapper;


	public class FilterPersonsViewPM
	{
		[Inject]
		[Bindable]
		public var personsModel:PersonsModel;

		private var _firstNameInputValue:String;

		[Bindable]
		public function get firstNameInputValue():String
		{
			return _firstNameInputValue;
		}

		public function set firstNameInputValue(value:String):void
		{
			if (_firstNameInputValue != value)
			{
				_firstNameInputValue = value;
				personsModel.filterModel.updateSearchValueByDataField(value, PersonVOMapper.COL_EMPLOYEE_FIRSTNAME);
			}
		}

		private var _lastNameInputValue:String;

		[Bindable]
		public function get lastNameInputValue():String
		{
			return _lastNameInputValue;
		}

		public function set lastNameInputValue(value:String):void
		{
			if (_lastNameInputValue != value)
			{
				_lastNameInputValue = value;
				personsModel.filterModel.updateSearchValueByDataField(value, PersonVOMapper.COL_EMPLOYEE_LASTNAME);
			}
		}

		private var _positionInputValue:Object;

		[Bindable]
		public function get positionInputValue():Object
		{
			return _positionInputValue;
		}

		public function set positionInputValue(value:Object):void
		{
			if (_positionInputValue != value)
			{
				_positionInputValue = value;

				var positionValue:String = positionInputValue ? positionInputValue[PersonVOMapper.COL_EMPLOYEE_POSITION] : "";
				personsModel.filterModel.updateSearchValueByDataField(positionValue, PersonVOMapper.COL_EMPLOYEE_POSITION);
			}
		}

		public function applyFilters():void
		{
			personsModel.searchPersons();
		}

		public function clearAllFilters():void
		{
			firstNameInputValue = "";
			lastNameInputValue = "";
			positionInputValue = null;

			applyFilters();
		}
	}
}
