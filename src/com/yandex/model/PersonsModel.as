package com.yandex.model
{
	import com.yandex.collections.PagedList;
	import com.yandex.events.ItemListModelEvent;
	import com.yandex.messages.NotificationMessage;
	import com.yandex.messages.OpenConnectionMessage;
	import com.yandex.messages.create.AddPersonMessage;
	import com.yandex.messages.del.DeleteAllPersonsMessage;
	import com.yandex.messages.del.DeletePersonsMessage;
	import com.yandex.messages.read.GetDepartmentPositionsMessage;
	import com.yandex.messages.read.GetPersonsCountMessage;
	import com.yandex.messages.read.GetPersonsMessage;
	import com.yandex.messages.update.MovePersonsMessage;
	import com.yandex.messages.update.MoveAllPersonsMessage;
	import com.yandex.model.vo.DepartmentVO;
	import com.yandex.model.vo.PersonVO;
	import com.yandex.model.vo.PersonVOMapper;

	import flash.data.SQLConnection;
	import flash.events.EventDispatcher;

	import mx.collections.ArrayList;
	import mx.collections.IList;
	import mx.collections.errors.ItemPendingError;
	import mx.events.CollectionEvent;
	import mx.utils.StringUtil;

	[Event(name="itemsChange", type="com.yandex.events.ItemListModelEvent")]
	[Event(name="selectionChange", type="com.yandex.events.ItemListModelEvent")]
	public class PersonsModel extends EventDispatcher
	{
		public static var SELECTED_RANGE:String = "selected";

		public static var ALL_RANGE:String = "all";

		private static var PERSONS_PAGE_SIZE:Number = 20;

		[Bindable]
		[Inject]
		public var filterModel:FilterModel;

		[MessageDispatcher]
		public var dispatcher:Function;

		[Bindable]
		public var selectedItems:IList;

		[Bindable]
		public var persons:IList;

		[Bindable]
		public var allDepartments:IList;

		[Bindable]
		public var positions:IList;

		[Bindable]
		public var displayPromptMessage:Boolean = true;

		[Bindable]
		public var promptMessage:String;

		[ResourceBinding(bundle="main",key="promptWelcomeMessage")]
		public var WELCOME_MESSAGE:String;

		[ResourceBinding(bundle="main",key="promptLoadingMessage")]
		public var LOADING_MESSAGE:String;

		[ResourceBinding(bundle="main",key="promptLongOperationMessage")]
		public var LONG_OPERATION_MESSAGE:String;

		[ResourceBinding(bundle="main",key="personsTotalCount")]
		public var TOTAL_COUNT_TEMPLATE:String;

		private var pageLoadRequestsCount:int;

		private var loadedPagesIndexes:Array;

		private var currentPageIndex:int;

		private var connection:SQLConnection;

		[Init]
		public function onInit():void
		{
			persons = new PagedList();
			persons.addEventListener(CollectionEvent.COLLECTION_CHANGE, onPersonsListChange);

			selectedItems = new ArrayList();
			selectedItems.addEventListener(CollectionEvent.COLLECTION_CHANGE, onSelectedItemsChange);

			loadedPagesIndexes = new Array();

			positions = new ArrayList();

			promptMessage = WELCOME_MESSAGE;
		}

		private function onPersonsListChange(event:CollectionEvent):void
		{
			dispatchEvent(new ItemListModelEvent(ItemListModelEvent.ITEMS_CHANGE));
		}

		private function onSelectedItemsChange(event:CollectionEvent):void
		{
			dispatchEvent(new ItemListModelEvent(ItemListModelEvent.SELECTION_CHANGE));
		}

		private var _department:DepartmentVO;

		[Bindable]
		public function get department():DepartmentVO
		{
			return _department;
		}

		public function set department(value:DepartmentVO):void
		{
			if (department != value)
			{
				_department = value;

				resetView();

				if (department)
				{
					loadPersonsCount(department.id);
					loadAllPersonPositions();
				}
				else
				{
					if (persons is PagedList)
					{
						PagedList(persons).totalCount = 0;
					}
				}
			}
		}

		public function selectPerson(selected:Boolean, person:PersonVO, rowIndex:int):void
		{
			var isPersonAlreadySelected:Boolean = isPersonSelected(person);

			if (selected && isPersonAlreadySelected ||
			    !selected && !isPersonAlreadySelected)
			{
				return;
			}

			if (selected)
			{
				selectedItems.addItem(person);
			}
			else
			{
				selectedItems.removeItemAt(selectedItems.getItemIndex(person))
			}
		}

		[Bindable("selectionChange")]
		public function isPersonSelected(person:PersonVO):Boolean
		{
			for (var i:int = 0; i < selectedItems.length; i++)
			{
				var selectedPerson:PersonVO = selectedItems.getItemAt(i) as PersonVO;
				if (selectedPerson.id == person.id)
				{
					return true;
				}
			}

			return false;
		}

		public function setSelection(selectedData:Vector.<int>):void
		{
			selectedItems.removeAll();

			for each (var selectedIndex:int in selectedData)
			{
				var person:PersonVO = persons.getItemAt(selectedIndex) as PersonVO;
				selectPerson(true, person, selectedIndex);
			}
		}

		internal function loadAllPersonPositions():void
		{
			dispatcher(new GetDepartmentPositionsMessage(connection, department.id, onLoadPersonPositionsResult));
		}

		public function addPerson(person:PersonVO):void
		{
			dispatcher(new AddPersonMessage(connection, person, onAddPersonResult));
		}

		public function movePersons(persons:Array, department:DepartmentVO):void
		{
			dispatcher(new MovePersonsMessage(connection, persons, department.id, onMovePersonsResult));
		}

		public function moveAllPersons(department:DepartmentVO):void
		{
			displayPromptMessage = true;
			promptMessage = LONG_OPERATION_MESSAGE;
			dispatcher(new MoveAllPersonsMessage(connection,
								              department.id,
					                          filterModel.searchFields,
					                          onMoveAllPersonsResult));
		}

		public function deletePersons(persons:Array):void
		{
			dispatcher(new DeletePersonsMessage(connection, persons, onDeletePersonsResult));
		}

		public function deleteAllPersons():void
		{

			displayPromptMessage = true;
			promptMessage = LONG_OPERATION_MESSAGE;
			dispatcher(new DeleteAllPersonsMessage(connection, department.id, onDeleteAllResult));
		}

		public function searchPersons():void
		{
			if (!department)
			{
				return;
			}

			resetView();
			loadPersonsCount(department.id);
		}

		private function loadPersonsCount(departmentID:Number):void
		{
			promptMessage = LOADING_MESSAGE;

			dispatcher(new GetPersonsCountMessage(connection,
					department.id,
					filterModel.searchFields,
					onGetPersonsCountResult));
		}

		private function resetView():void
		{
			currentPageIndex = -1;
			persons.removeAll();
			selectedItems.removeAll();
			loadedPagesIndexes = new Array();
		}

		[Bindable("itemsChange")]
		public function get displayedTotalCount():String
		{
			return StringUtil.substitute(TOTAL_COUNT_TEMPLATE, persons.length);
		}

		public function createPendingItemFunctionHandler(index:int, ipe:ItemPendingError):Object
		{
			var pageIndexToLoad:int = int (index / PERSONS_PAGE_SIZE);
			loadPersonsPage(pageIndexToLoad);

			return null;
		}

		private function loadPersonsPage(pageIndex:Number):void
		{
			if (!department ||
			    !persons.length ||
				loadedPagesIndexes.indexOf(pageIndex) != -1)
			{
				return;
			}

			loadedPagesIndexes.push(pageIndex);

			pageLoadRequestsCount++;

			displayPromptMessage = true;
			promptMessage = LOADING_MESSAGE;

			dispatcher(new GetPersonsMessage(connection,
					department.id,
					pageIndex * PERSONS_PAGE_SIZE,
					PERSONS_PAGE_SIZE,
					filterModel.searchFields,
					onLoadPersonsResult));
		}

		private function setPersons(personsData:Array, offset:Number):void
		{
			for (var index:int = 0; index < personsData.length; index++)
			{
				var person:Object = personsData[index];

				var personVO:PersonVO = PersonVOMapper.toPersonVO(person);
				persons.setItemAt(personVO, offset + index);
			}
		}

		/* Result handlers*/

		private function onAddPersonResult(person:PersonVO):void
		{
			if (person.departmentID == department.id)
			{
				persons.addItem(person);

				var notificationParameters:Array = [person.firstName  + " " + person.lastName];
				var notification:Notification  = new Notification(NotificationContent.PERSON_ADD, notificationParameters);
				dispatcher(new NotificationMessage(notification));
			}
		}

		private function onMovePersonsResult(movedPersons:Array):void
		{
			if (!movedPersons || !movedPersons.length)
			{
				return;
			}

			//TODO:Shall be something smarter compared to total view reset
			resetView();
			loadPersonsCount(department.id);

			var notificationParameters:Array;
			var notification:Notification;

			if (persons.length == 1)
			{
				var person:PersonVO = movedPersons[0] as PersonVO;
				notificationParameters = [person.firstName  + " " + person.lastName];
				notification  = new Notification(NotificationContent.PERSON_UPDATE, notificationParameters);
				dispatcher(new NotificationMessage(notification));
			}
			else
			{
				notificationParameters = [movedPersons.length];
				notification  = new Notification(NotificationContent.PERSONS_UPDATE, notificationParameters);
				dispatcher(new NotificationMessage(notification));
			}
		}

		private function onMoveAllPersonsResult():void
		{
			var movedPersonCount:Number = persons.length;
			displayPromptMessage = false;
			promptMessage = "";

			//TODO:Shall be something smarter compared to total view reset
			resetView();
			loadPersonsCount(department.id);

			var notificationParameters:Array;
			var notification:Notification;

			if (movedPersonCount == 1)
			{
				notificationParameters = [movedPersonCount];
				notification  = new Notification(NotificationContent.PERSON_UPDATE, notificationParameters);
				dispatcher(new NotificationMessage(notification));
			}
			else
			{
				notificationParameters = [movedPersonCount];
				notification  = new Notification(NotificationContent.PERSONS_UPDATE, notificationParameters);
				dispatcher(new NotificationMessage(notification));
			}
		}

		private function onDeleteAllResult():void
		{
			displayPromptMessage = false;
			promptMessage = "";

			var notificationParameters:Array;
			var notification:Notification;
			if (persons.length == 1)
			{
				var person:PersonVO = persons[0] as PersonVO;
				notificationParameters = [person.firstName  + " " + person.lastName];
				notification  = new Notification(NotificationContent.PERSON_REMOVE, notificationParameters);
				dispatcher(new NotificationMessage(notification));
			}
			else
			{
				notificationParameters = [persons.length];
				notification  = new Notification(NotificationContent.PERSONS_REMOVE, notificationParameters);
				dispatcher(new NotificationMessage(notification));
			}

			//TODO:Shall be something smarter compared to total view reset
			resetView();
			loadPersonsCount(department.id);
		}

		private function onDeletePersonsResult(deletedPersons:Array):void
		{
			if (!deletedPersons || !deletedPersons.length)
			{
				return;
			}

			var notificationParameters:Array;
			var notification:Notification;
			if (deletedPersons.length == 1)
			{
				var person:PersonVO = deletedPersons[0] as PersonVO;
				notificationParameters = [person.firstName  + " " + person.lastName];
				notification  = new Notification(NotificationContent.PERSON_REMOVE, notificationParameters);
				dispatcher(new NotificationMessage(notification));
			}
			else
			{
				notificationParameters = [deletedPersons.length];
				notification  = new Notification(NotificationContent.PERSONS_REMOVE, notificationParameters);
				dispatcher(new NotificationMessage(notification));
			}


			//TODO:Shall be something smarter compared to total view reset
			resetView();
			loadPersonsCount(department.id);
		}

		private function onLoadPersonsResult(personsData:Array, offset:Number):void
		{
			pageLoadRequestsCount--;
			displayPromptMessage = (pageLoadRequestsCount > 0);

			currentPageIndex = int(offset / PERSONS_PAGE_SIZE);

			if (!personsData)
			{
				return;
			}

			setPersons(personsData, offset);
		}

		private function onGetPersonsCountResult(personCount:int):void
		{
			if (persons is PagedList)
			{
				PagedList(persons).totalCount = personCount;
			}
			loadPersonsPage(0);
		}

		private function onLoadPersonPositionsResult(positionsData:Array):void
		{
		    positions = new ArrayList(positionsData);
		}

		private function onLoadDepartmentHeadCountResult(departmentID:Number, departmentPersonsCount:int):void
		{
			if (persons is PagedList)
			{
				PagedList(persons).totalCount = departmentPersonsCount;
			}

			loadPersonsPage(0);
		}

		/*
			 Message handlers
		 */
		[MessageHandler]
		public function onOpenConnection(message:OpenConnectionMessage):void
		{
			connection = message.connection;
		}
	}
}

