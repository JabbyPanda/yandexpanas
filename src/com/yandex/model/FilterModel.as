package com.yandex.model
{
	import com.yandex.model.search.SearchField;

	import flash.events.Event;

	import flash.events.EventDispatcher;

	public class FilterModel extends EventDispatcher
	{
		public var searchFields:Array;

		public function updateSearchValueByDataField(searchKey:String, dataFieldName:String):void
		{
			var searchField:SearchField = getSearchFieldByName(dataFieldName);
			if (searchField)
			{
				searchField.value = searchKey;
				dispatchEvent(new Event("searchFieldChanged"));
			}
		}

		[Bindable("searchFieldChanged")]
		public function getSearchFieldByName(dataFieldName:String):SearchField
		{
			for each (var searchField:SearchField in searchFields)
			{
				if (searchField.dataFieldName == dataFieldName)
				{
					return searchField;
				}
			}

			return null;
		}
	}
}
