package com.yandex.model
{
	import com.yandex.messages.OpenConnectionMessage;

	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;

	public class SQLConnectionModel
	{
		[MessageDispatcher]
		public var dispatcher:Function;

		private var connection:SQLConnection;

		private var databaseFile:File;

		[Init]
		public function onInit():void
		{
			var embeddedSessionDB:File = File.applicationDirectory.resolvePath("staff.db");
			databaseFile = File.applicationStorageDirectory.resolvePath("staff.db");
			if (!databaseFile.exists)
			{
				embeddedSessionDB.copyTo(databaseFile);
			}

			openConnection(databaseFile);
		}

		public function openConnection(database:File):void
		{
			connection = new SQLConnection();
			connection.addEventListener(SQLEvent.OPEN, openConnectionHandler);
			connection.addEventListener(SQLErrorEvent.ERROR, errorConnectionHandler);
			connection.openAsync(database, SQLMode.UPDATE);
		}

		private function openConnectionHandler(event:SQLEvent):void
		{
			dispatcher(new OpenConnectionMessage(connection));
		}

		private function errorConnectionHandler(event:SQLErrorEvent):void
		{

		}
	}
}
