package com.yandex.model
{
	import mx.utils.StringUtil;
	import mx.utils.UIDUtil;

	public class Notification
	{
		public function Notification(message:String, parameters:Array = null)
		{
			this.id = UIDUtil.createUID();
			if (parameters)
			{
				this.message = StringUtil.substitute(message, parameters)
			}
			else
			{
				this.message = message;
			}
		}

		public var id:String;

		public var message:String;
	}
}
