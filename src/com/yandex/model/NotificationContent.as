package com.yandex.model
{
	public class NotificationContent
	{
		public static const PERSON_ADD:String = "Added {0} person";

		public static const PERSON_REMOVE:String = "Removed {0} person";

		public static const PERSONS_REMOVE:String = "Removed {0} persons";

		public static const DEPARTMENT_ADD:String = "Added {0} department";

		public static const DEPARTMENT_REMOVE:String = "Removed {0} department";

		public static const PERSON_UPDATE:String = "Updated {0} person";

		public static const PERSONS_UPDATE:String = "Updated {0} persons";
	}
}
