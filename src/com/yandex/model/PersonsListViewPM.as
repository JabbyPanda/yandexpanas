package com.yandex.model
{
	import spark.components.supportClasses.DropDownController;
	import spark.events.DropDownEvent;

	public class PersonsListViewPM
	{
		public var ddc:DropDownController = new DropDownController();

		[Init]
		public function onInit():void
		{
			ddc.closeOnResize = false;
		}
	}
}
