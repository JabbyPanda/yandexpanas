package com.yandex.model
{
	import com.yandex.messages.NotificationMessage;

	import flash.events.Event;
	import flash.events.EventDispatcher;

	import mx.collections.ArrayCollection;

	public class NotificationsModel extends EventDispatcher
	{
		public var notifications:ArrayCollection;

		[Bindable("notificationsChanged")]
		public function get lastNotificationMessage():String
		{
			return hasAnyNotification
					? (notifications.getItemAt(notifications.length -1) as Notification).message
					: "No notifications";
		}

		[Bindable("notificationsChanged")]
		public function get hasAnyNotification():Boolean
		{
			return notifications && notifications.length;
		}

		public function addNotification(notification:Notification):void
		{
			notifications.addItem(notification);
			dispatchEvent (new Event("notificationsChanged"));
		}

		public function removeLastNotification():void
		{
			if (!notifications.length)
			{
				return
			}

			notifications.source.shift();
			dispatchEvent (new Event("notificationsChanged"));
		}

		/*
		    Message handlers
		 */
		[MessageHandler]
		public function onNotificationMessageReceived(message:NotificationMessage):void
		{
			notifications ||= new ArrayCollection();
			addNotification(message.notification);
		}
	}
}
