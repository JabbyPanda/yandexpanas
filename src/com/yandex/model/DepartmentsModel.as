package com.yandex.model
{
	import com.yandex.events.ItemListModelEvent;
	import com.yandex.messages.NotificationMessage;
	import com.yandex.messages.OpenConnectionMessage;
	import com.yandex.messages.create.AddDepartmentMessage;
	import com.yandex.messages.del.DeleteDepartmentMessage;
	import com.yandex.messages.read.GetDepartmentsMessage;
	import com.yandex.model.vo.DepartmentVO;
	import com.yandex.model.vo.DepartmentVOMapper;

	import flash.data.SQLConnection;
	import flash.events.EventDispatcher;

	import mx.collections.ArrayCollection;
	import mx.collections.IList;
	import mx.events.CollectionEvent;

	[Event(name="itemsChange", type="com.yandex.events.ItemListModelEvent")]
	[Event(name="selectionChange", type="com.yandex.events.ItemListModelEvent")]
	public class DepartmentsModel extends EventDispatcher
	{
		[Bindable]
		public var departments:IList = new ArrayCollection();

		[MessageDispatcher]
		public var dispatcher:Function;

		[Init]
		public function onInit():void
		{
			departments = new ArrayCollection();
			departments.addEventListener(CollectionEvent.COLLECTION_CHANGE, onDepartmentsListChange)
		}

		private function onDepartmentsListChange(event:CollectionEvent):void
		{
			dispatchEvent(new ItemListModelEvent(ItemListModelEvent.ITEMS_CHANGE));
		}

		private var connection:SQLConnection;

		private var _selectedDepartment:DepartmentVO;

		[Bindable("selectionChange")]
		public function get selectedDepartment():DepartmentVO
		{
			return _selectedDepartment;
		}

		public function set selectedDepartment(department:DepartmentVO):void
		{
			if (selectedDepartment != department)
			{
				_selectedDepartment = department;
				dispatchEvent(new ItemListModelEvent(ItemListModelEvent.SELECTION_CHANGE));
			}
		}

		public function loadDepartments():void
		{
			dispatcher(new GetDepartmentsMessage(connection, onLoadDepartmentsResult));
		}

		public function addDepartment(department:DepartmentVO):void
		{
			department.id = departments.length + 1;
			dispatcher(new AddDepartmentMessage(connection, department, onAddDepartmentResult));
		}

		public function removeDepartment(department:DepartmentVO):void
		{
			dispatcher(new DeleteDepartmentMessage(connection, department, onDeleteDepartmentResult));
		}

		private function getDepartmentByID(departmentID:Number):DepartmentVO
		{
			for each (var department:DepartmentVO in departments)
			{
				if (department.id == departmentID)
				{
					return department;
				}
			}

			return null;
		}

		/* Result handlers*/

		private function onAddDepartmentResult(department:DepartmentVO):void
		{
			departments.addItem(department);
			var notificationParameters:Array = [department.name];
			dispatcher(new NotificationMessage(new Notification(NotificationContent.DEPARTMENT_ADD, notificationParameters)));
		}

		private function onDeleteDepartmentResult(department:DepartmentVO):void
		{
			departments.removeItemAt(departments.getItemIndex(department));
			var notificationParameters:Array = [department.name];
			dispatcher(new NotificationMessage(new Notification(NotificationContent.DEPARTMENT_REMOVE, notificationParameters)));

			if (selectedDepartment == department)
			{
				selectedDepartment = null;
			}
		}

		private function onLoadDepartmentsResult(loadedDepartments:Array):void
		{
			for each (var department:Object in loadedDepartments)
			{
				var departmentVO:DepartmentVO = DepartmentVOMapper.toDepartmentVO(department);
				departments.addItem(departmentVO);
			}
		}

		/*
			Message handlers
		 */
		[MessageHandler]
		public function onOpenConnection(message:OpenConnectionMessage):void
		{
			connection = message.connection;
			loadDepartments();
		}
	}
}
