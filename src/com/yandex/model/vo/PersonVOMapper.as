package com.yandex.model.vo
{
	public class PersonVOMapper
	{
		public static const COL_EMPLOYEE_ID:String = "id";
		public static const COL_EMPLOYEE_DEPARTMENT_ID:String = "departmentID";
		public static const COL_EMPLOYEE_FIRSTNAME:String = "firstName";
		public static const COL_EMPLOYEE_LASTNAME:String = "lastName";
		public static const COL_EMPLOYEE_POSITION:String = "position";

		public static function toPersonVO(person:Object):PersonVO
		{
			var personVO:PersonVO = new PersonVO();
			personVO.id           = person[PersonVOMapper.COL_EMPLOYEE_ID];
			personVO.departmentID = person[PersonVOMapper.COL_EMPLOYEE_DEPARTMENT_ID];
			personVO.firstName    = person[PersonVOMapper.COL_EMPLOYEE_FIRSTNAME];
			personVO.lastName     = person[PersonVOMapper.COL_EMPLOYEE_LASTNAME];
			personVO.position     = person[PersonVOMapper.COL_EMPLOYEE_POSITION];

			return personVO;
		}
	}
}
