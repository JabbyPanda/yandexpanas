package com.yandex.model.vo
{
	public class PersonVO
	{
		public var id:int;

		public var departmentID:int;

		public var firstName:String;

		public var lastName:String;

		public var position:String;

		[Transient]
		public var selected:Boolean;

		[Transient]
		public var index:Number;
	}
}