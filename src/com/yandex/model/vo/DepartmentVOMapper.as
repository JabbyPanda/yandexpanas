package com.yandex.model.vo
{
	public class DepartmentVOMapper
	{
		public static const COL_DEPARTMENT_ID:String = "id";
		public static const COL_DEPARTMENT_NAME:String = "name";
		//public static const COL_DEPARTMENT_HEADCOUNT:String = "headCount";

		public static function toDepartmentVO(department:Object):DepartmentVO
		{
			var departmentVO:DepartmentVO = new DepartmentVO();
			departmentVO.id = department[DepartmentVOMapper.COL_DEPARTMENT_ID];
			departmentVO.name = department[DepartmentVOMapper.COL_DEPARTMENT_NAME];

			return departmentVO;
		}
	}
}
