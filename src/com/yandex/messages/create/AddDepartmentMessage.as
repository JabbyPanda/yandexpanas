package com.yandex.messages.create
{
	import com.yandex.messages.RequestMessage;
	import com.yandex.model.vo.DepartmentVO;

	import flash.data.SQLConnection;

	public class AddDepartmentMessage extends RequestMessage
	{
		private var _department:DepartmentVO;

		public function get department():DepartmentVO
		{
			return _department;
		}

		public function AddDepartmentMessage(connection:SQLConnection,
		                                     department:DepartmentVO,
		                                     resultHandler:Function,
		                                     errorHandler:Function = null)
		{
			super(connection, resultHandler, errorHandler);
			_department = department;
		}
	}
}
