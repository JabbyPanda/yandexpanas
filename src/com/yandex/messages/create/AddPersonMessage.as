package com.yandex.messages.create
{
	import com.yandex.messages.RequestMessage;
	import com.yandex.model.vo.PersonVO;

	import flash.data.SQLConnection;

	public class AddPersonMessage extends RequestMessage
	{
		private var _person:PersonVO;

		public function get person():PersonVO
		{
			return _person;
		}

		public function AddPersonMessage(connection:SQLConnection,
		                                     person:PersonVO,
		                                     resultHandler:Function,
		                                     errorHandler:Function = null)
		{
			super(connection, resultHandler, errorHandler);
			_person = person;
		}
	}
}
