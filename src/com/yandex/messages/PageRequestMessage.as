package com.yandex.messages
{
	import flash.data.SQLConnection;

	public class PageRequestMessage extends RequestMessage
	{
		private var _offsetIndex:Number;

		public function get offsetIndex():Number
		{
			return _offsetIndex;
		}

		private var _pageSize:Number;

		public function get pageSize():Number
		{
			return _pageSize;
		}

		public function PageRequestMessage(connection:SQLConnection,
		                                   offsetIndex:Number,
		                                   pageSize:Number,
		                                   resultHandler:Function = null,
		                                   errorHandler:Function = null)
		{
			super(connection, resultHandler, errorHandler);
			_offsetIndex = offsetIndex;
			_pageSize = pageSize;
		}
	}
}
