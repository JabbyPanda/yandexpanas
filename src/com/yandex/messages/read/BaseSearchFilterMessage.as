
package com.yandex.messages.read
{
	import com.yandex.messages.RequestMessage;
	import com.yandex.model.search.SearchField;

	import flash.data.SQLConnection;

	public class BaseSearchFilterMessage extends RequestMessage
	{
		private var _searchFields:Array;

		public function get searchFields():Array
		{
			return _searchFields;
		}

		public function get hasNotEmptySearchFields():Boolean
		{
			for each (var searchField:SearchField in searchFields)
			{
				if (searchField.value)
				{
					return true;
				}
			}

			return false;
		}

		public function BaseSearchFilterMessage(connection:SQLConnection,
		                                        searchFields:Array,
		                                        resultHandler:Function = null,
		                                        errorHandler:Function = null)
		{
			super(connection, resultHandler, errorHandler);

			_searchFields = searchFields;
		}
	}
}
