package com.yandex.messages.read
{
	import com.yandex.messages.RequestMessage;

	import flash.data.SQLConnection;

	public class GetDepartmentPositionsMessage extends RequestMessage
	{
		private var _departmentID:Number;

		public function get departmentID():Number
		{
			return _departmentID;
		}

		public function GetDepartmentPositionsMessage(connection:SQLConnection,
		                                          departmentID:Number,
		                                          resultHandler:Function,
		                                          errorHandler:Function = null)
		{
			_departmentID = departmentID;
			super(connection, resultHandler, errorHandler)
		}
	}
}
