package com.yandex.messages.read
{
	import com.yandex.messages.PageRequestMessage;
	import com.yandex.model.search.SearchField;

	import flash.data.SQLConnection;

	public class GetPersonsMessage extends PageRequestMessage
	{
		private var _departmentID:Number;

		public function get departmentID():Number
		{
			return _departmentID;
		}

		private var _searchFields:Array;

		public function get searchFields():Array
		{
			return _searchFields;
		}

		public function get hasNotEmptySearchFields():Boolean
		{
			for each (var searchField:SearchField in searchFields)
			{
				if (searchField.value)
				{
					return true;
				}
			}

			return false;
		}

		public function GetPersonsMessage(connection:SQLConnection,
		                                  departmentID:Number,
		                                  offsetIndex:Number,
		                                  pageSize:Number,
		                                  searchFields:Array,
		                                  resultHandler:Function,
		                                  errorHandler:Function = null)
		{
			super(connection, offsetIndex, pageSize,resultHandler, errorHandler);

			_departmentID = departmentID;
			_searchFields = searchFields;
		}
	}
}