package com.yandex.messages.read
{
	import flash.data.SQLConnection;

	public class GetPersonsCountMessage extends BaseSearchFilterMessage
	{
		private var _departmentID:Number;

		public function get departmentID():Number
		{
			return _departmentID;
		}

		public function GetPersonsCountMessage(connection:SQLConnection,
		                                       departmentID:Number,
		                                       searchFields:Array,
		                                       resultHandler:Function,
		                                       errorHandler:Function = null)
		{
			_departmentID = departmentID;
			super(connection, searchFields, resultHandler, errorHandler)
		}
	}
}
