package com.yandex.messages.read
{
	import com.yandex.messages.RequestMessage;

	import flash.data.SQLConnection;

	public class GetDepartmentsMessage extends RequestMessage
	{
		public function GetDepartmentsMessage(connection:SQLConnection,
		                                      resultHandler:Function,
		                                      errorHandler:Function = null)
		{
			super(connection, resultHandler, errorHandler);
		}
	}
}
