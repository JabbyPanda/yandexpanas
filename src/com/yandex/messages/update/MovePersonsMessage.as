package com.yandex.messages.update
{
	import com.yandex.messages.RequestMessage;
	import com.yandex.model.vo.PersonVO;

	import flash.data.SQLConnection;

	public class MovePersonsMessage extends RequestMessage
	{
		private var _persons:Array;

		public function get persons():Array
		{
			return _persons;
		}

		private var _newDepartmentID:Number;

		public function get newDepartmentID():Number
		{
			return _newDepartmentID;
		}

		public function MovePersonsMessage(connection:SQLConnection,
		                                   persons:Array,
		                                   departmentID:Number,
		                                   resultHandler:Function,
		                                   errorHandler:Function = null)
		{
			super(connection, resultHandler, errorHandler);
			_persons = persons;
			_newDepartmentID = departmentID;
		}
	}
}
