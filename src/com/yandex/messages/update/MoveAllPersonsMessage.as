package com.yandex.messages.update
{
	import com.yandex.messages.read.BaseSearchFilterMessage;

	import flash.data.SQLConnection;

	public class MoveAllPersonsMessage extends BaseSearchFilterMessage
	{
		private var _departmentID:Number;

		public function get departmentID():Number
		{
			return _departmentID;
		}

		public function MoveAllPersonsMessage(connection:SQLConnection,
		                                   departmentID:Number,
		                                    searchFields:Array,
		                                    resultHandler:Function,
		                                    errorHandler:Function = null)
		{
			super(connection, searchFields, resultHandler, errorHandler);
			_departmentID = departmentID;
		}
	}
}
