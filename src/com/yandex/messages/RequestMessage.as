package com.yandex.messages
{
	import flash.data.SQLConnection;

	public class RequestMessage
	{
		private var _resultHandler:Function;

		public function get resultHandler():Function
		{
			return _resultHandler;
		}

		private var _errorHandler:Function;


		public function get errorHandler():Function
		{
			return _errorHandler;
		}

		private var _connection:SQLConnection;

		public function get connection():SQLConnection
		{
			return _connection;
		}

		public function RequestMessage(connection:SQLConnection, resultHandler:Function = null, errorHandler:Function = null)
		{
			_connection = connection;
			_resultHandler = resultHandler;
			_errorHandler = errorHandler;
		}
	}
}
