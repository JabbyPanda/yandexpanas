package com.yandex.messages
{
	import com.yandex.model.Notification;

	public class NotificationMessage
	{
		private var _notification:Notification;

		public function get notification():Notification
		{
			return _notification;
		}

		public function NotificationMessage(notification:Notification)
		{
			_notification = notification;
		}
	}
}
