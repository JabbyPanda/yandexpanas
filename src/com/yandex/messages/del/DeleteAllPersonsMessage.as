package com.yandex.messages.del
{
	import com.yandex.messages.RequestMessage;

	import flash.data.SQLConnection;

	public class DeleteAllPersonsMessage extends RequestMessage
	{
		private var _departmentID:int;

		public function get departmentID():int
		{
			return _departmentID;
		}

		public function DeleteAllPersonsMessage(connection:SQLConnection,
		                                        department:int,
		                                        resultHandler:Function,
		                                        errorHandler:Function = null)
		{
			super(connection, resultHandler, errorHandler);
			_departmentID = department;
		}
	}
}
