package com.yandex.messages.del
{
	import com.yandex.messages.RequestMessage;
	import com.yandex.model.vo.DepartmentVO;

	import flash.data.SQLConnection;

	public class DeleteDepartmentMessage extends RequestMessage
	{
		private var _department:DepartmentVO;

		public function get department():DepartmentVO
		{
			return _department;
		}

		public function DeleteDepartmentMessage(connection:SQLConnection,
		                                     department:DepartmentVO,
		                                     resultHandler:Function,
		                                     errorHandler:Function = null)
		{
			super(connection, resultHandler, errorHandler);
			_department = department;
		}
	}
}
