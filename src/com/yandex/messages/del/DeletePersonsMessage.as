package com.yandex.messages.del
{
	import com.yandex.messages.RequestMessage;

	import flash.data.SQLConnection;

	public class DeletePersonsMessage extends RequestMessage
	{
		private var _persons:Array;

		public function get persons():Array
		{
			return _persons;
		}

		public function DeletePersonsMessage(connection:SQLConnection,
		                                    persons:Array,
		                                    resultHandler:Function,
		                                    errorHandler:Function = null)
		{
			super(connection, resultHandler, errorHandler);
			_persons = persons;
		}
	}
}
