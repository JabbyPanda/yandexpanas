package com.yandex.messages
{
	import flash.data.SQLConnection;

	public class OpenConnectionMessage
	{
		private var _connection:SQLConnection;
		public function get connection():SQLConnection
		{
			return _connection;
		}

		public function OpenConnectionMessage(connection:SQLConnection)
		{
			_connection = connection;
		}
	}
}
