package com.yandex.events
{
	import flash.events.Event;

	public class ItemListModelEvent extends Event
	{
		public static const ITEMS_CHANGE:String = "itemsChange";

		public static const SELECTION_CHANGE:String = "selectionChange";

		public function ItemListModelEvent(type:String)
		{
			super(type);
		}
	}
}
