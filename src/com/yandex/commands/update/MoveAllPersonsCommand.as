package com.yandex.commands.update
{
	import com.yandex.commands.BasePersonsSearchCommand;
	import com.yandex.messages.update.MoveAllPersonsMessage;

	import flash.data.SQLResult;
	import flash.events.SQLEvent;

	public class MoveAllPersonsCommand extends BasePersonsSearchCommand
	{
		[MessageDispatcher]
		public var dispatcher:Function;

		private static const SQL:String =
				"UPDATE main.Employees \n" +
				"SET DeptID=:deptID \n";

		private var messageResultHandler:Function;

		public function execute(message:MoveAllPersonsMessage):void
		{
			var sql:String = SQL;
			messageResultHandler = message.resultHandler;

			if (message.hasNotEmptySearchFields)
			{
				sql += parseSearchFields(message.searchFields, false);
			}

			statement.sqlConnection = message.connection;
			statement.text = sql;
			statement.parameters[":deptID"] =  message.departmentID;

			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			var result:SQLResult = statement.getResult();
			messageResultHandler();
		}
	}
}
