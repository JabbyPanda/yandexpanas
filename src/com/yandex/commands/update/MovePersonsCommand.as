package com.yandex.commands.update
{
	import com.yandex.commands.*;
	import com.yandex.messages.update.MovePersonsMessage;
	import com.yandex.model.vo.PersonVO;

	import flash.data.SQLResult;
	import flash.events.SQLEvent;

	import mx.utils.StringUtil;

	public class MovePersonsCommand extends BaseSQLCommand
	{
		[MessageDispatcher]
		public var dispatcher:Function;

		private static const SQL:String =
				"UPDATE main.Employees \n" +
				"SET DeptID=:deptID \n" +
				"WHERE EmplID IN ({0})";

		private var messageResultHandler:Function;

		private var persons:Array;

		public function execute(message:MovePersonsMessage):void
		{
			persons = message.persons;
			messageResultHandler = message.resultHandler;

			statement.sqlConnection = message.connection;
			var movedIDList:String = MultiplePersonIDUtil.createIDList(message.persons);
			statement.text = StringUtil.substitute(SQL, movedIDList);
			statement.parameters[":deptID"] = message.newDepartmentID;

			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			var result:SQLResult = statement.getResult();
			messageResultHandler(persons);
		}
	}
}
