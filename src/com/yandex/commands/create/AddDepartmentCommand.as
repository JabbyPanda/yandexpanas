package com.yandex.commands.create
{
	import com.yandex.commands.*;
	import com.yandex.messages.create.AddDepartmentMessage;
	import com.yandex.model.vo.DepartmentVO;

	import flash.data.SQLResult;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;

	public class AddDepartmentCommand extends BaseSQLCommand
	{
		[MessageDispatcher]
		public var dispatcher:Function;


		private static const SQL:String =
				"INSERT INTO main.Departments" +
						"(DeptID, DeptName)"  +
						" VALUES (:deptID, :deptName)";

		private var messageResultHandler:Function;

		private var department:DepartmentVO;

		public function execute(message:AddDepartmentMessage):void
		{
			department = message.department;
			messageResultHandler = message.resultHandler;

			statement.sqlConnection = message.connection;
			statement.text = SQL;
			statement.parameters[":deptID"] =  department.id;
			statement.parameters[":deptName"] = department.name;

			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			var result:SQLResult = statement.getResult();

			department.id = result.lastInsertRowID;
			messageResultHandler(department);
		}
	}
}
