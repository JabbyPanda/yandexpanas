package com.yandex.commands.create
{
	import com.yandex.commands.*;
	import com.yandex.messages.create.AddPersonMessage;
	import com.yandex.model.vo.PersonVO;

	import flash.data.SQLResult;
	import flash.events.SQLEvent;

	public class AddPersonCommand extends BaseSQLCommand
	{
		[MessageDispatcher]
		public var dispatcher:Function;

		private static const SQL:String =
				"INSERT INTO main.Employees" +
						"(EmplID, DeptID, FirstName, LastName, Position)"  +
						" VALUES (:emplID, :deptID, :firstName, :lastName, :position)";

		private var messageResultHandler:Function;

		private var person:PersonVO;

		public function execute(message:AddPersonMessage):void
		{
			person = message.person;
			messageResultHandler = message.resultHandler;

			statement.sqlConnection = message.connection;
			statement.text = SQL;
			statement.parameters[":emplID"]    = person.id;
			statement.parameters[":deptID"]    = person.departmentID;
			statement.parameters[":firstName"] = person.firstName;
			statement.parameters[":lastName"]  = person.lastName;
			statement.parameters[":position"]  = person.position;

			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			var result:SQLResult = statement.getResult();

			person.id = result.lastInsertRowID;
			messageResultHandler(person);
		}
	}
}
