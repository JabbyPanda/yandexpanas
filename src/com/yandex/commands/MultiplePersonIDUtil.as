package com.yandex.commands
{
	import com.yandex.model.vo.PersonVO;

	public class MultiplePersonIDUtil
	{
		public static function createIDList(persons:Array):String
		{
			var idList:String = "";
			var index:int = 0;
			while (index < persons.length)
			{
				var person:PersonVO = persons[index] as PersonVO;
				idList += person.id;
				if (index < persons.length -1)
				{
					idList += ", ";
				}
				index++;
			}

			return idList;
		}
	}
}
