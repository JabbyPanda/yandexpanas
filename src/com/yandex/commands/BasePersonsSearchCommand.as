package com.yandex.commands
{
	import com.yandex.model.search.SearchField;
	import com.yandex.model.vo.PersonVOMapper;

	public class BasePersonsSearchCommand extends BaseSQLCommand
	{
		protected static const WHERE_CLAUSE_SQL:String = "WHERE ";

		protected static const AND_CLAUSE_SQL:String = "AND ";

		protected static const FIRST_NAME_SQL:String = "FirstName LIKE :firstName \n";

		protected  static const LAST_NAME_SQL:String = "LastName LIKE :lastName \n";

		protected  static const POSITION_SQL:String = "Position LIKE :position \n";

		protected function parseSearchFields(searchFields:Array, continueWhereClause:Boolean = true):String
		{
			var searchFieldsSQL:String = continueWhereClause ? AND_CLAUSE_SQL : WHERE_CLAUSE_SQL;
			var notEmptySearchFieldValues:Array = [];
			for each (var searchField:SearchField in searchFields)
			{
				if (searchField.value)
				{
					switch (searchField.dataFieldName)
					{
						case PersonVOMapper.COL_EMPLOYEE_FIRSTNAME:
						{
							notEmptySearchFieldValues.push(FIRST_NAME_SQL);
							statement.parameters[":firstName"] = '%' + searchField.value + '%';
							break;
						}
						case PersonVOMapper.COL_EMPLOYEE_LASTNAME:
						{
							notEmptySearchFieldValues.push(LAST_NAME_SQL);
							statement.parameters[":lastName"] = '%' + searchField.value + '%';
							break;
						}
						case PersonVOMapper.COL_EMPLOYEE_POSITION:
						{
							notEmptySearchFieldValues.push(POSITION_SQL);
							statement.parameters[":position"] = '%' + searchField.value + '%';
							break;
						}
					}
				}
			}
			return searchFieldsSQL.concat(notEmptySearchFieldValues.join(AND_CLAUSE_SQL));
		}
	}
}
