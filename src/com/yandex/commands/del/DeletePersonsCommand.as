package com.yandex.commands.del
{
	import com.yandex.commands.*;
	import com.yandex.messages.del.DeletePersonsMessage;
	import com.yandex.model.vo.PersonVO;

	import flash.data.SQLResult;
	import flash.events.SQLEvent;

	import mx.utils.StringUtil;

	public class DeletePersonsCommand extends BaseSQLCommand
	{
		[MessageDispatcher]
		public var dispatcher:Function;

		private static const SQL:String = "DELETE FROM main.Employees WHERE EmplID IN ({0})";

		private var messageResultHandler:Function;

		private var persons:Array;

		public function execute(message:DeletePersonsMessage):void
		{
			persons = message.persons;
			messageResultHandler = message.resultHandler;

			statement.sqlConnection = message.connection;

			var deletedIDList:String = MultiplePersonIDUtil.createIDList(message.persons);
			statement.text = StringUtil.substitute(SQL, deletedIDList);
			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			var result:SQLResult = statement.getResult();
			messageResultHandler(persons);
		}
	}
}
