package com.yandex.commands.del
{
	import com.yandex.commands.*;
	import com.yandex.messages.del.DeleteDepartmentMessage;
	import com.yandex.model.vo.DepartmentVO;

	import flash.data.SQLResult;
	import flash.events.SQLEvent;

	public class DeleteDepartmentCommand extends BaseSQLCommand
	{
		[MessageDispatcher]
		public var dispatcher:Function;

		private static const SQL:String = "DELETE FROM main.Departments WHERE DeptID=:deptID";

		private var messageResultHandler:Function;

		private var department:DepartmentVO;

		public function execute(message:DeleteDepartmentMessage):void
		{
			department = message.department;
			messageResultHandler = message.resultHandler;

			statement.sqlConnection = message.connection;
			statement.text = SQL;
			statement.parameters[":deptID"] =  department.id;

			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			messageResultHandler(department);
		}
	}
}
