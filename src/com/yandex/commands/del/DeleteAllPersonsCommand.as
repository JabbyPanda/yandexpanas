package com.yandex.commands.del
{
	import com.yandex.commands.*;
	import com.yandex.messages.del.DeleteAllPersonsMessage;

	import flash.data.SQLResult;
	import flash.events.SQLEvent;

	public class DeleteAllPersonsCommand extends BaseSQLCommand
	{
		[MessageDispatcher]
		public var dispatcher:Function;

		private static const SQL:String = "DELETE FROM main.Employees WHERE DeptID=:deptID";

		private var messageResultHandler:Function;

		public function execute(message:DeleteAllPersonsMessage):void
		{

			messageResultHandler = message.resultHandler;

			statement.sqlConnection = message.connection;
			statement.text = SQL;
			statement.parameters[":deptID"] =  message.departmentID;
			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			var result:SQLResult = statement.getResult();
			messageResultHandler();
		}
	}
}
