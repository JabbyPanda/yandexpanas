package com.yandex.commands.read
{
	import com.yandex.commands.*;
	import com.yandex.messages.read.GetPersonsCountMessage;

	import flash.data.SQLResult;
	import flash.events.SQLEvent;

	import mx.utils.StringUtil;

	public class GetPersonsCountCommand extends BasePersonsSearchCommand
	{
		private static const SQL:String =
				"SELECT COUNT(EmplID) as personsCount \n" +
						"FROM main.Employees \n" +
						"WHERE DeptID={0} ";

		private var messageResultHandler:Function;

		public function execute(message:GetPersonsCountMessage):void
		{
			messageResultHandler = message.resultHandler;
			var sql:String =  StringUtil.substitute(SQL, message.departmentID);

			if (message.hasNotEmptySearchFields)
			{
				sql += parseSearchFields(message.searchFields);
			}

			statement.sqlConnection = message.connection;
			statement.text = sql;
			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			var result:SQLResult = statement.getResult();
			messageResultHandler(result.data[0].personsCount);
		}
	}
}
