package com.yandex.commands.read
{
	import com.yandex.commands.BasePersonsSearchCommand;
	import com.yandex.messages.read.GetPersonsMessage;
	import com.yandex.model.vo.PersonVOMapper;

	import flash.data.SQLResult;
	import flash.events.SQLEvent;

	import mx.utils.StringUtil;

	public class GetPersonsCommand extends BasePersonsSearchCommand
	{
		private static const SQL:String =
				"SELECT EmplID AS " + PersonVOMapper.COL_EMPLOYEE_ID + ",\n" +
						"DeptID AS " + PersonVOMapper.COL_EMPLOYEE_DEPARTMENT_ID + ",\n" +
						"FirstName AS " + PersonVOMapper.COL_EMPLOYEE_FIRSTNAME + ",\n" +
						"LastName AS " + PersonVOMapper.COL_EMPLOYEE_LASTNAME + ",\n" +
						"Position AS " + PersonVOMapper.COL_EMPLOYEE_POSITION + "\n" +
						"FROM main.Employees " + "\n" +
						"WHERE DeptID={0} " + "\n";

		private static const PAGE_LIMIT_SQL:String = " LIMIT :pageSize OFFSET :offset";

		private var messageResultHandler:Function;

		private var indexOffset:Number;

		public function execute(message:GetPersonsMessage):void
		{
			messageResultHandler = message.resultHandler;
			var sql:String =  StringUtil.substitute(SQL, message.departmentID);

			if (message.hasNotEmptySearchFields)
			{
				sql += parseSearchFields(message.searchFields);
			}

			sql += PAGE_LIMIT_SQL;
			statement.sqlConnection = message.connection;
			statement.text = sql;
			statement.parameters[":pageSize"] =  message.pageSize;
			statement.parameters[":offset"] = indexOffset = message.offsetIndex;

			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			var result:SQLResult = statement.getResult();
			messageResultHandler(result.data, indexOffset);
		}
	}
}
