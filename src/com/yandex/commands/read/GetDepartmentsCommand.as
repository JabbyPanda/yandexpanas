package com.yandex.commands.read
{
	import com.yandex.commands.*;
	import com.yandex.messages.read.GetDepartmentsMessage;
	import com.yandex.model.vo.DepartmentVOMapper;

	import flash.data.SQLResult;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;

	public class GetDepartmentsCommand extends BaseSQLCommand
	{
		[MessageDispatcher]
		public var dispatcher:Function;

		private static const SQL:String =
				"SELECT d.DeptID AS " + DepartmentVOMapper.COL_DEPARTMENT_ID + "," + "\n" +
						"d.DeptName AS " + DepartmentVOMapper.COL_DEPARTMENT_NAME + "\n" +
						"FROM main.Departments d";

		private var messageResultHandler:Function;

		public function GetDepartmentsCommand()
		{
			statement.text = SQL;
		}

		public function execute(message:GetDepartmentsMessage):void
		{
			messageResultHandler = message.resultHandler;
			statement.sqlConnection = message.connection;
			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			var result:SQLResult = statement.getResult();
			messageResultHandler(result.data);
		}
	}
}
