package com.yandex.commands.read
{
	import com.yandex.commands.*;
	import com.yandex.messages.read.GetDepartmentPositionsMessage;
	import com.yandex.model.vo.PersonVOMapper;

	import flash.data.SQLResult;
	import flash.events.SQLEvent;

	import mx.utils.StringUtil;

	public class GetDepartmentPositionsCommand extends BaseSQLCommand
	{
		private static const SQL:String =
				"SELECT e.Position AS "+ PersonVOMapper.COL_EMPLOYEE_POSITION + "\n" +
						"FROM main.Employees e \n" +
						"WHERE e.DeptID={0} \n";
						"GROUP BY Position";

		private var messageResultHandler:Function;

		public function execute(message:GetDepartmentPositionsMessage):void
		{
			messageResultHandler = message.resultHandler;

			statement.sqlConnection = message.connection;
			statement.text = StringUtil.substitute(SQL, message.departmentID);
			statement.execute();
		}

		override protected function resultHandler(event:SQLEvent):void
		{
			var result:SQLResult = statement.getResult();
			messageResultHandler(result.data);
		}
	}
}
