package com.yandex.commands
{
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;

	public class BaseSQLCommand
	{
		protected var statement:SQLStatement;

		public function BaseSQLCommand()
		{
			statement = new SQLStatement();
			statement.addEventListener(SQLEvent.RESULT, resultHandler);
			statement.addEventListener(SQLErrorEvent.ERROR, errorHandler);
		}

		protected function resultHandler(event:SQLEvent):void
		{
			//to be overriden
		}

		protected function errorHandler(event:SQLErrorEvent):void
		{
			trace ("error in sql request " +  event.error.details)
		}
	}
}
