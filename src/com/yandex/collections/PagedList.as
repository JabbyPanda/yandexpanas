package com.yandex.collections
{
	import mx.collections.ArrayList;
	import mx.collections.errors.ItemPendingError;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;

	public class PagedList extends ArrayList
	{
		public function PagedList(source:Array=null)
		{
			super(source);
		}

		public function set totalCount(value:int):void
		{
			const oldLength:int = length;
			const newLength:int = value;

			var ce:CollectionEvent = new CollectionEvent(CollectionEvent.COLLECTION_CHANGE);

			if (oldLength < newLength)
			{
				if (ce)
				{
					ce.location = Math.max(oldLength - 1, 0);
					ce.kind = CollectionEventKind.ADD;
					const itemsLength:int = newLength - oldLength;
					for (var i:int = 0; i < itemsLength; i++)
						ce.items.push(null);
				}

				source.length = newLength;
				for (var newIndex:int = Math.max(oldLength - 1, 0); newIndex < newLength; newIndex++)
				{
					source[newIndex] = null;
				}
			}
			else // oldLength > newLength
			{
				if (ce)
				{
					ce.location = Math.max(newLength - 1, 0);
					ce.kind = CollectionEventKind.REMOVE;
					for (var oldIndex:int = Math.max(newLength - 1, 0); oldIndex < oldLength; oldIndex++)
					{
						ce.items.push(source[oldIndex]);
					}
				}

				source.length = newLength;
			}

			if (ce)
				dispatchEvent(ce);
		}

		override public function getItemAt(index:int, prefetch:int=0):Object
		{
			var item:Object = super.getItemAt(index, prefetch);
			if (!item)
				throw new ItemPendingError("Item is not loaded yet");

			return item;
		}
	}
}