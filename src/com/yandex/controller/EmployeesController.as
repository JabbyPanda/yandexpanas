package com.yandex.controller
{
	import com.yandex.events.ItemListModelEvent;
	import com.yandex.model.DepartmentsModel;
	import com.yandex.model.PersonsModel;
	import com.yandex.model.FilterModel;
	import com.yandex.model.vo.DepartmentVO;

	public class EmployeesController
	{
		[Inject]
		public var personsModel:PersonsModel;

		[Inject]
		public var personsSearchModel:FilterModel;

		[Inject]
		public var departmentsModel:DepartmentsModel;

		[Init]
		public function onInit():void
		{
			departmentsModel.addEventListener(ItemListModelEvent.SELECTION_CHANGE, onDepartmentSelected);
			departmentsModel.addEventListener(ItemListModelEvent.ITEMS_CHANGE, onDepartmentsListChange);
		}

		private function onDepartmentsListChange(event:ItemListModelEvent):void
		{
			personsModel.allDepartments = departmentsModel.departments;
		}

		private function onDepartmentSelected(event:ItemListModelEvent):void
		{
			personsModel.department = departmentsModel.selectedDepartment;
		}
	}
}
