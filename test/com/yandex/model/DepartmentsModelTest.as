package com.yandex.model
{
	import com.yandex.messages.create.AddDepartmentMessage;
	import com.yandex.messages.del.DeleteDepartmentMessage;
	import com.yandex.model.vo.DepartmentVO;

	import org.flexunit.asserts.assertTrue;
	import org.hamcrest.assertThat;
	import org.hamcrest.object.equalTo;

	public class DepartmentsModelTest
	{
		private var departmentsModel:DepartmentsModel;

		[Before]
		public function setUp():void
		{
			departmentsModel = new DepartmentsModel();
			fillDepartments();
		}

		private function fillDepartments(count:Number = 10):void
		{
			departmentsModel.dispatcher = function(message:*):void
			{
				if (message is AddDepartmentMessage)
				{
					message.resultHandler(message.department);
				}
			};

			for (var i:int = 1; i < count + 1; i++)
			{
				var department:DepartmentVO = new DepartmentVO();
				department.id = i;
				department.name = "test" + i;

				departmentsModel.addDepartment(department);
			}
		}

		[Test]
		public function testRemoveDepartment():void
		{
			 //given
			departmentsModel.dispatcher = function(message:*):void
			{
				if (message is DeleteDepartmentMessage)
				{
					assertThat(message.department.name, equalTo('test1'));
					message.resultHandler(message.department);
				}
			};
			var firstItem:DepartmentVO = departmentsModel.departments.getItemAt(0) as DepartmentVO;

			//when
			departmentsModel.removeDepartment(firstItem);

			//then
			assertTrue(departmentsModel.departments.length == 9);
		}

		[Test]
		public function testSelectDepartment():void
		{

		}

		[Test]
		public function testAddDepartment():void
		{

		}
	}
}
