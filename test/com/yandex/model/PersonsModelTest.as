package com.yandex.model
{
	import com.yandex.collections.PagedList;
	import com.yandex.messages.create.AddPersonMessage;
	import com.yandex.model.vo.DepartmentVO;
	import com.yandex.model.vo.PersonVO;

	import mx.collections.ArrayList;

	import org.flexunit.asserts.assertTrue;
	import org.hamcrest.assertThat;
	import org.hamcrest.object.equalTo;

	public class PersonsModelTest
	{

		public var personsModel:PersonsModel;

		[Before]
		public function setUp():void
		{
		 	personsModel = new PersonsModel();
			personsModel.persons = new PagedList();
			personsModel.selectedItems = new ArrayList();
			personsModel.filterModel = new FilterModel();
		}

		[Test]
		public function testAddPerson():void
		{
			//given
			personsModel.dispatcher = function(message:*):void
			{
				if (message is AddPersonMessage)
				{
					assertThat(message.person.firstName, equalTo('test1'));
					message.resultHandler(person);
				}
			};

			personsModel.department = createDepartment(1);
			var person:PersonVO = createPerson(1, personsModel.department.id);

			//when
			personsModel.addPerson(person);

			//then
			assertTrue(personsModel.persons.length == 1);
		}

		[Test]
		public function testDeleteAllPersons():void
		{

		}

		[Test]
		public function testMovePersons():void
		{

		}

		[Test]
		public function testDeletePersons():void
		{

		}

		[Test]
		public function testSearchPersons():void
		{

		}

		[Test]
		public function testSelectPerson():void
		{

		}

		[Test]
		public function testMoveAllPersons():void
		{

		}


		private function createDepartment(index:int):DepartmentVO
		{
			var department:DepartmentVO = new DepartmentVO();
			department.id = index;
			department.name = "test" + index;

			return department;
		}

		private function createPerson(index:Number, departmentID:Number):PersonVO
		{
			var person:PersonVO = new PersonVO();

			person.id = index;
			person.departmentID = departmentID;
			person.firstName = "test" + index;
			person.lastName = "test" + index;
			person.position = "test" + index;
			return person;
		}
	}
}
